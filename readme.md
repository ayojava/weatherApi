# Weather forecast API

* Clone the project to an IDE (preferably intelliJ IDEA)
* Open the application.yml file and change the database credentials for a mysql database (username , password and url)

```yaml
datasource:
    username: root
    password: ayojava
    url: jdbc:mysql://127.0.0.1/weatherDB?useSSL=false

```

* Also in the application.yml file the cron scheduler is currently configured to run 6 am & 7pm  daily , however in order to shorten the time period and run , change to this
which runs the application every 10seconds
```yaml
cron:
  format:
    value: 0/10 * * * * *
```

* The below configuration is used to check which temperature to monitor . it checks for records which if the temperature for that day will be lower than 
the "minimum" temperature or higher than the "maximum" temperature and records it as part of the forecast . This means that any day (based on the configured location)
that the temperature of that day will be lower than the "minimum" temperature i:e "0" or the temperature will be higher than the "maximum" i:e "100",
the data will be logged 

```yaml
tempconfig:
  minTemperature : 0
  maxTemperature : 150
```

* Current cities been checked include Majzar , Somalia , Helsinki . This can be retrieved from the "listOfCities.json" file in "resources/json" folder.
You can add more by checking up the actual city available.
```yaml
locations :
    - Majzar
    - Somalia
    - Helsinki
```

* from the IDE , click on the "run" arrow to start the application

* tests can also be run from the IDE (The test uses h2DB which is initially populated with data from populateData.sql file in the test/resources folder)

*once the application is up , with postman , send a GET request to retrieve the details of the logged data
```
http://localhost:8080/weatherForecast/get?cityName=Helsinki&cityId=658226
```
* Either cityName or cityId must be present , else an error will be returned 

```json
{
    "code": "001",
    "message": "City Name or city id is required",
    "exceptionTime": "2021-11-09T20:12:21.034972"
}
```

* A successful response returns the below json 
```json
{
    "cityId": 658226,
    "cityName": "Helsinki",
    "latitude": 60.1756,
    "longitude": 24.9342,
    "temperatureLogList": [
        {
            "temperatureDateTime": "2021-11-14T12:00:00",
            "actualTemperature": {
                "morningTemperature": 0.62,
                "dayTemperature": 1.06,
                "eveningTemperature": -0.07,
                "nightTemperature": -0.35,
                "minimumTemperature": -0.41,
                "maximumTemperature": 1.44
            },
            "feelsLikeTemperature": {
                "morningTemperature": -3.34,
                "dayTemperature": -2.46,
                "eveningTemperature": -3.67,
                "nightTemperature": -3.78,
                "minimumTemperature": null,
                "maximumTemperature": null
            }
        },
        {
            "temperatureDateTime": "2021-11-15T12:00:00",
            "actualTemperature": {
                "morningTemperature": -1.01,
                "dayTemperature": 1.09,
                "eveningTemperature": 2.51,
                "nightTemperature": 4.23,
                "minimumTemperature": -1.01,
                "maximumTemperature": 4.23
            },
            "feelsLikeTemperature": {
                "morningTemperature": -1.01,
                "dayTemperature": -2.16,
                "eveningTemperature": -2.38,
                "nightTemperature": -0.85,
                "minimumTemperature": null,
                "maximumTemperature": null
            }
        }
    ]
}
```