package org.javasoft.weatherapi

import mu.KotlinLogging
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Assertions.assertNull
import org.junit.jupiter.api.Assertions.assertTrue
import org.junit.jupiter.api.BeforeAll
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.jdbc.Sql
import org.springframework.test.context.jdbc.SqlGroup
import org.springframework.test.context.junit4.SpringRunner

private val log = KotlinLogging.logger {}

@ActiveProfiles("test")
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
 class WeatherApiApplicationTests(@Autowired val testRestTemplate: TestRestTemplate , @Autowired private val  weatherProviderFactory: WeatherProviderFactory) {

    @LocalServerPort
    private  var serverPort: Int = 0

    fun createUrlWithPort(uri : String): String = "http://localhost:$serverPort/$uri"

    @Test
    @Sql(scripts = ["classpath:populateData.sql"], executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    fun `test get weather forecast should return 200`(){
        val path = "/weatherForecast/get?cityId=34717"
        val responseEntity = testRestTemplate.exchange(createUrlWithPort(path), HttpMethod.GET, null, WeatherForecastResponse::class.java)
        assertThat(responseEntity.statusCodeValue).isEqualTo(HttpStatus.OK.value())
    }

    @Test
    fun `test get weather forecast with invalid data should return 400`(){
        val path = "/weatherForecast/get?cityId=2344111"
        val responseEntity = testRestTemplate.exchange(createUrlWithPort(path), HttpMethod.GET, null, WeatherForecastResponse::class.java)
        assertThat(responseEntity.statusCodeValue).isEqualTo(HttpStatus.BAD_REQUEST.value())
    }

    @Test
    fun `test get weather forecast without query params should return 400`(){
        val path = "/weatherForecast/get"
        val responseEntity = testRestTemplate.exchange(createUrlWithPort(path), HttpMethod.GET, null, WeatherForecastResponse::class.java)
        assertThat(responseEntity.statusCodeValue).isEqualTo(HttpStatus.BAD_REQUEST.value())
    }

    @Test
    fun `test with wrong provider should return null`(){
        assertNull(weatherProviderFactory.getWeatherProvider("INVALID"),"INVALID provider name should return null")
    }

}


