package org.javasoft.weatherapi


import org.junit.jupiter.api.Assertions
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.jdbc.Sql
import java.util.*


@ActiveProfiles("test")
@SpringBootTest( webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
class WeatherApiSpecificationTests(@Autowired val weatherRecordRepository: WeatherRecordRepository) {


    @Test
    @Sql(scripts = ["classpath:populateData.sql"], executionPhase = Sql.ExecutionPhase.BEFORE_TEST_METHOD)
    fun `test records in the db that it is not empty`(){
        val weatherRecordSpecification = WeatherRecordSpecification.buildWeatherRecordSpecification(34717,null)
        val weatherEntityList = weatherRecordRepository.findAll(weatherRecordSpecification)
        Assertions.assertNotEquals(Collections.EMPTY_LIST, weatherEntityList);
    }

    @Test
    fun `test records in the db that it is  empty`(){
        val weatherRecordSpecification = WeatherRecordSpecification.buildWeatherRecordSpecification(34707,null)
        val weatherEntityList = weatherRecordRepository.findAll(weatherRecordSpecification)
        Assertions.assertEquals(Collections.EMPTY_LIST, weatherEntityList);
    }
}