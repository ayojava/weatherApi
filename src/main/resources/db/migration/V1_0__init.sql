create table if not exists temperaturerecord
(
    id bigint auto_increment
        primary key,
    day_temperature double not null,
    evening_temperature double not null,
    maximum_temperature double null,
    minimum_temperature double null,
    morning_temperature double not null,
    night_temperature double not null,
    temperature_enum varchar(255) not null
)
    engine=MyISAM;

create table if not exists weather_temperature
(
    weather_id bigint not null,
    temperature_id bigint not null,
    constraint temperature_constraint unique (temperature_id)
)
    engine=MyISAM;

create index weather_index on weather_temperature (weather_id);

create table if not exists weatherrecord
(
    id bigint auto_increment
        primary key,
    city_id bigint not null,
    city_name varchar(255) not null,
    latitude double not null,
    location_date datetime not null,
    longitude double not null
)
    engine=MyISAM;

