package org.javasoft.weatherapi

import mu.KotlinLogging
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime


private val logger = KotlinLogging.logger {}

@Service
class SchedulerService(private val weatherFacade: WeatherFacade){

    @Scheduled(cron = "\${cron.format.value}")
    fun weatherCheckSchedule(){
        logger.info { "Time is  ${LocalDateTime.now()}" }
        weatherFacade.connectToWeatherService(ProviderEnums.OPEN_WEATHER_MAP.providerName)
    }
}