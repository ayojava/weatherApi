package org.javasoft.weatherapi

import org.springframework.data.jpa.domain.Specification
import javax.persistence.criteria.CriteriaBuilder
import javax.persistence.criteria.CriteriaQuery
import javax.persistence.criteria.Predicate
import javax.persistence.criteria.Root

object WeatherRecordSpecification {

    fun buildWeatherRecordSpecification(cityId : Long? , cityName : String?  ): Specification<WeatherRecordEntity?> {
        return Specification<WeatherRecordEntity?> { root: Root<WeatherRecordEntity?>, query: CriteriaQuery<*>, cb: CriteriaBuilder ->
            val predicates: MutableList<Predicate> = ArrayList()
            if(cityId != null){
                val searchByCityIdPredicate = cb.and(cb.equal(root.get<Long>("cityId"), cityId))
                predicates.add(searchByCityIdPredicate)
            }
            if(cityName != null){
                val searchByCityNamePredicate = cb.and(cb.equal(root.get<String>("cityName"), cityName))
                predicates.add(searchByCityNamePredicate)
            }


            cb.and(*predicates.toTypedArray())
        }
    }
}