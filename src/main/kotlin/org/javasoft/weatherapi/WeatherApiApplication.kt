package org.javasoft.weatherapi

import mu.KotlinLogging
import org.springframework.boot.CommandLineRunner
import org.springframework.boot.autoconfigure.SpringBootApplication
import org.springframework.boot.context.properties.ConfigurationPropertiesScan
import org.springframework.boot.context.properties.EnableConfigurationProperties
import org.springframework.boot.runApplication
import org.springframework.context.annotation.Bean
import org.springframework.scheduling.annotation.EnableScheduling
import org.springframework.scheduling.annotation.Scheduled
import org.springframework.stereotype.Service
import java.time.LocalDateTime

private val logger = KotlinLogging.logger {}

@EnableScheduling
@SpringBootApplication
@ConfigurationPropertiesScan
class WeatherApiApplication{



//    @Bean
//    fun testConnect(openWeatherMapProvider: OpenWeatherMapProvider) =  CommandLineRunner{
//        openWeatherMapProvider.connectToWebService()
//    }
//
//    @Bean
//    fun testConnect(weatherFacade: WeatherFacade) =  CommandLineRunner{
//        weatherFacade.connectToWeatherService(ProviderEnums.OPEN_WEATHER_MAP.providerName)
//    }

}




fun main(args: Array<String>) {
    runApplication<WeatherApiApplication>(*args)
}
