package org.javasoft.weatherapi

import java.time.LocalDateTime
import javax.persistence.*

@Entity
@Table(name = "weatherrecord")
data class WeatherRecordEntity (

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long ? = null,

    val longitude : Double,

    val latitude : Double,

    @OneToMany(fetch = FetchType.EAGER , cascade = [CascadeType.ALL])
    @JoinTable(name = "weather_temperature",
        joinColumns = arrayOf(JoinColumn(name = "weather_id")),
        inverseJoinColumns = arrayOf(JoinColumn(name="temperature_id")))
    val temperatureRecordList : List<TemperatureRecordEntity>,

    @Column(nullable = false)
    val cityName : String ,

    val cityId : Long,

    val locationDate : LocalDateTime
    )

@Entity
@Table(name = "temperaturerecord")
data class TemperatureRecordEntity(

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    val id: Long ? = null,

    val morningTemperature : Double,

    val dayTemperature : Double,

    val eveningTemperature : Double,

    val nightTemperature : Double,

    val minimumTemperature : Double ? = null,

    val maximumTemperature : Double ? = null,

    @Enumerated(EnumType.STRING)
    val temperatureEnum: TemperatureEnum
)