package org.javasoft.weatherapi

import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler

class WeatherApiException(var errorResponse: ErrorResponse):RuntimeException()

@ControllerAdvice
class GlobalExceptionHandler {

    @ExceptionHandler(WeatherApiException::class)
    fun handleWeatherApiException(weatherApiException: WeatherApiException): ResponseEntity<ErrorResponse> {
        return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(weatherApiException.errorResponse)
    }
}