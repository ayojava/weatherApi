package org.javasoft.weatherapi

import com.fasterxml.jackson.annotation.JsonIgnoreProperties
import java.time.LocalDateTime
import javax.validation.constraints.Min

data class Location(val latitude : Double , val longitude : Double , val cityName : String, val cityId : Long)

data class Coord(val lon: Double , val lat : Double)

data class City(val id : Long ,val name : String ,val state : String ,val country : String ,val coord : Coord )

data class Weather ( val id : Int,  val main : String, val description : String, val icon : String)

data class Temp (

    val day : Double,
    val min : Double,
    val max : Double,
    val night : Double,
    val eve : Double,
    val morn : Double
)

data class Daily (
    val dt : Int,
    val sunrise : Int,
    val sunset : Int,
    val moonrise : Int,
    val moonset : Int,
    val moon_phase : Double,
    val temp : Temp,
    val feels_like : Feels_like,
    val pressure : Int,
    val humidity : Int,
    val dew_point : Double,
    val wind_speed : Double,
    val wind_deg : Int,
    val wind_gust : Double,
    val weather : List<Weather>,
    val clouds : Int,
    val pop : Double,
    val uvi : Double
)

data class Feels_like (
    val day : Double,
    val night : Double,
    val eve : Double,
    val morn : Double
)

data class OpenWeatherResponseError(val cod : Int , val message : String)

data class ErrorResponse(val code : String ,val message :String , val exceptionTime : LocalDateTime = LocalDateTime.now() )


data class OpenWeatherResponsePayload (

    val lat : Double,
    val lon : Double,
    val timezone : String,
    val timezone_offset : Int,
    val daily : List<Daily>
)

data class WeatherForecastRequest(
    @field:Min(value = 0 ,message = "cityId cannot be less than zero")
    var cityId : Long? , var cityName : String?)

data class TemperatureData(val morningTemperature : Double, val dayTemperature : Double,val eveningTemperature : Double, val nightTemperature : Double, val minimumTemperature : Double ? = null, val maximumTemperature : Double ? = null)

data class TemperatureLog(val temperatureDateTime : LocalDateTime , var actualTemperature : TemperatureData? = null , var feelsLikeTemperature : TemperatureData ? = null)

@JsonIgnoreProperties(ignoreUnknown = true)
data class WeatherForecastResponse(var cityId : Long? , var cityName : String? ,var latitude: Double? , var longitude: Double? ,var temperatureLogList : List<TemperatureLog>?)