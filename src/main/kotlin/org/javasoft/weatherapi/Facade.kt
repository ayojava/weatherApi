package org.javasoft.weatherapi

import mu.KotlinLogging
import org.springframework.stereotype.Service

private val logger = KotlinLogging.logger {}

@Service
class WeatherFacade(private val weatherProviderFactory: WeatherProviderFactory, val weatherDbService: WeatherDbService) {

    fun connectToWeatherService(providerName : String){
        val abstractWeatherProvider = weatherProviderFactory.getWeatherProvider(providerName)
        if(abstractWeatherProvider != null){
            logger.info { "Retrieved WeatherProvider ${abstractWeatherProvider.getName()} with status ${abstractWeatherProvider.isActive()}" }
            abstractWeatherProvider.connectToWebService()
        }else{
            logger.error { "No Weather Provider found with given name $providerName" }
        }
    }

    fun getWeatherForecastList(weatherForecastRequest: WeatherForecastRequest): WeatherForecastResponse {
        if(weatherForecastRequest.cityId == null && weatherForecastRequest.cityName.isNullOrEmpty()){
            throw WeatherApiException(ErrorResponse("001","City Name or city id is required"))
        }
        val weatherEntityList: List<WeatherRecordEntity>? = weatherDbService.getWeatherEntityList(weatherForecastRequest)
        if(weatherEntityList != null && weatherEntityList.isNotEmpty()){
            val temperatureLogList = mutableListOf<TemperatureLog>()
            for(aWeatherEntity in weatherEntityList){
                val temperatureLog = TemperatureLog(aWeatherEntity.locationDate)
                aWeatherEntity.temperatureRecordList.forEach {
                    if(it.temperatureEnum == TemperatureEnum.FEELS_LIKE){
                        temperatureLog.feelsLikeTemperature =TemperatureData(morningTemperature = it.morningTemperature,dayTemperature = it.dayTemperature,eveningTemperature = it.eveningTemperature,nightTemperature = it.nightTemperature)
                    }else{
                        temperatureLog.actualTemperature = TemperatureData(morningTemperature = it.morningTemperature,dayTemperature = it.dayTemperature,eveningTemperature = it.eveningTemperature,nightTemperature = it.nightTemperature,minimumTemperature = it.minimumTemperature,maximumTemperature = it.maximumTemperature)
                    }
                }
                temperatureLogList.add(temperatureLog)
            }
            val (_, longitude, latitude, _, cityName, cityId, _) = weatherEntityList[0]
            return WeatherForecastResponse(cityId,cityName,latitude,longitude,temperatureLogList)
        }else{
            throw WeatherApiException(ErrorResponse("900","record Not found"))
        }
    }

}
