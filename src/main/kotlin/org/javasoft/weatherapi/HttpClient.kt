package org.javasoft.weatherapi

import com.fasterxml.jackson.databind.DeserializationFeature
import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.json.JsonParser
import org.springframework.http.HttpMethod
import org.springframework.http.HttpStatus
import org.springframework.web.client.RestTemplate

private val logger = KotlinLogging.logger {}

class OpenWeatherHttpClient(private val restTemplate: RestTemplate) {

    private val objectMapper = jacksonObjectMapper()

    @Autowired
    private lateinit var jsonParser: JsonParser

    fun doGetRequest(path : String) : OpenWeatherResponsePayload? {
        val responseEntity = restTemplate.exchange(path, HttpMethod.GET, null, String::class.java)
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false)
        when(responseEntity.statusCode){
            HttpStatus.OK -> {
                val jsonObjectMap: MutableMap<String, Any> = jsonParser.parseMap(responseEntity.body)
                for((key,value) in jsonObjectMap){
                    if(key.equals("cod",true) && value == 429){
                        //we had an error , just log it
                        val openWeatherResponseError = objectMapper.readValue(responseEntity.body, OpenWeatherResponseError::class.java)
                        logger.error { "An error has occurred $openWeatherResponseError" }
                    }else{
                        val openWeatherResponsePayload = objectMapper.readValue(responseEntity.body, OpenWeatherResponsePayload::class.java)
                        logger.info { "Got a successful response ::: " }
                        return openWeatherResponsePayload
                    }
                }
            }
            else ->{
                logger.error { "An error has occurred " }
            }
        }
        return null
    }
}