package org.javasoft.weatherapi

import com.fasterxml.jackson.module.kotlin.jacksonObjectMapper
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Value
import org.springframework.core.io.Resource
import org.springframework.stereotype.Component
import java.io.BufferedReader
import javax.annotation.PostConstruct

private val logger = KotlinLogging.logger {}

@Component
class StartUpUtil(@Value("classpath:json/listOfCities.json")val cityListResourceFile: Resource,val openWeatherConfig: OpenWeatherConfig) {

    private val objectMapper = jacksonObjectMapper()

    lateinit var locationsList : List<Location>


    @PostConstruct
    fun loadCitiesJson(){
        val citiesJsonString: String = cityListResourceFile.inputStream.bufferedReader().use(BufferedReader::readText)
        val citiesList: List<City> = objectMapper.readValue(citiesJsonString, Array<City>::class.java).toList()


        //this assumes that the name of the location will  be unique in the data , might be wrong though
        val citiesMap: Map<String, City> = citiesList.associateBy { it.name }

        locationsList = openWeatherConfig.locations.map { Location(citiesMap[it]?.coord?.lat!!, citiesMap[it]?.coord?.lon!!, citiesMap[it]?.name!!,citiesMap[it]?.id!!) }

    }
}