package org.javasoft.weatherapi

import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding


@ConstructorBinding
@ConfigurationProperties(prefix = "openweatherconfig")
data class OpenWeatherConfig(val apiKey:  String , val baseUrl : String , val active : Boolean , val locations : List<String>)

@ConstructorBinding
@ConfigurationProperties(prefix = "tempconfig")
data class TempConfig(val minTemperature : Double , val maxTemperature : Double)