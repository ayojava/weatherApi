package org.javasoft.weatherapi

enum class TemperatureEnum {

    ACTUAL,
    FEELS_LIKE
}