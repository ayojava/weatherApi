package org.javasoft.weatherapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.*

@RestController
@RequestMapping("/weatherForecast")
class WeatherForecastController(@Autowired val weatherFacade: WeatherFacade) {

    @GetMapping("/get")
    fun getWeatherForecastRecord(@ModelAttribute weatherForecastRequest: WeatherForecastRequest): ResponseEntity<WeatherForecastResponse> {
        val weatherForecastResponse = weatherFacade.getWeatherForecastList(weatherForecastRequest)
        return ResponseEntity(weatherForecastResponse,HttpStatus.OK)
    }

    @PostMapping("/connect")
    fun connectToWeatherService() : ResponseEntity<Void>{
        weatherFacade.connectToWeatherService(ProviderEnums.OPEN_WEATHER_MAP.providerName)
        return ResponseEntity<Void>(HttpStatus.OK)
    }
}