package org.javasoft.weatherapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.context.properties.ConfigurationProperties
import org.springframework.boot.context.properties.ConstructorBinding
import org.springframework.boot.json.JsonParser
import org.springframework.boot.json.JsonParserFactory
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.context.annotation.Bean
import org.springframework.context.annotation.Configuration
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler



@Configuration
class BeanConfig {

    @Bean
    fun openWeatherHttpClient(@Autowired restTemplateBuilder: RestTemplateBuilder): OpenWeatherHttpClient {
        return OpenWeatherHttpClient(OpenWeatherRestTemplateFactory(restTemplateBuilder).openWeatherRestTemplate)
    }

    @Bean
    fun jsonParser(): JsonParser {
        return JsonParserFactory.getJsonParser()
    }

    @Bean
    fun threadPoolTaskScheduler(): ThreadPoolTaskScheduler? {
        val threadPoolTaskScheduler = ThreadPoolTaskScheduler()
        threadPoolTaskScheduler.poolSize = 5
        threadPoolTaskScheduler.setThreadNamePrefix("ThreadPoolTaskScheduler")
        return threadPoolTaskScheduler
    }
}