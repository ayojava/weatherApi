package org.javasoft.weatherapi

import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.apache.commons.lang3.StringUtils
import org.springframework.beans.factory.annotation.Value
import org.springframework.boot.json.JsonParser
import org.springframework.core.io.Resource
import java.io.BufferedReader
import java.time.Instant
import java.time.LocalDateTime
import java.time.ZoneId
import java.util.*


private val logger = KotlinLogging.logger {}

interface WeatherProvider{

    fun isActive(): Boolean

    fun getName(): String

    fun connectToWebService()
}

abstract class AbstractWeatherProvider  : WeatherProvider{

    //inject repository to save record to db
    @Autowired
    private lateinit var  weatherDbService: WeatherDbService

    fun handleResponse(openWeatherResponsePayload : OpenWeatherResponsePayload,minTemperature : Double ,maxTemperature : Double , cityName : String, cityId : Long ){

        //check for temperature less than minimum or greater than maximum
        val dailyList  = openWeatherResponsePayload.daily.filter { (it.temp.min < minTemperature) || (it.temp.max > maxTemperature) }

        dailyList.forEach {
            val (day, min, max, night, eve, morn) = it.temp
            val (day_, night_, eve_, morn_) = it.feels_like
            val temperatureRecordList = listOf( TemperatureRecordEntity( morningTemperature = morn,  dayTemperature = day, eveningTemperature = eve,  nightTemperature = night, minimumTemperature = min, maximumTemperature = max, temperatureEnum = TemperatureEnum.ACTUAL),
                TemperatureRecordEntity(morningTemperature = morn_, dayTemperature = day_, eveningTemperature = eve_, nightTemperature = night_, temperatureEnum = TemperatureEnum.FEELS_LIKE))

            val localDateTime = LocalDateTime.ofInstant(Instant.ofEpochSecond(it.dt.toLong()), ZoneId.systemDefault());

            val weatherRecordEntity = WeatherRecordEntity(longitude = openWeatherResponsePayload.lon,latitude = openWeatherResponsePayload.lat , temperatureRecordList = temperatureRecordList ,cityName = cityName , locationDate = localDateTime , cityId = cityId)
            weatherDbService.saveWeatherEntity(weatherRecordEntity)
        }

    }
}


@Service
class OpenWeatherMapProvider(private val openWeatherConfig: OpenWeatherConfig ,
                             private val openWeatherHttpClient: OpenWeatherHttpClient,
                             private val tempConfig : TempConfig,
                             private val startUpUtil: StartUpUtil) : AbstractWeatherProvider() {

    val apiKey = openWeatherConfig.apiKey

    override fun isActive(): Boolean {
        return openWeatherConfig.active
    }

    override fun getName(): String {
        return ProviderEnums.OPEN_WEATHER_MAP.providerName
    }

    override fun connectToWebService() {
        for (aLocation in startUpUtil.locationsList){
            val latitude = aLocation.latitude
            val longitude = aLocation.longitude
            val path = "/data/2.5/onecall?lat=${latitude}&lon=${longitude}&exclude=current,minutely,hourly,alerts&appid=${apiKey}&units=metric"
            val openWeatherResponsePayload = openWeatherHttpClient.doGetRequest(openWeatherConfig.baseUrl.plus(path))
            if(openWeatherResponsePayload != null){
                handleResponse(openWeatherResponsePayload!! , tempConfig.minTemperature, tempConfig.maxTemperature , aLocation.cityName , aLocation.cityId)
            }
        }
        logger.info { "completed" }
    }




}

