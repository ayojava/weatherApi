package org.javasoft.weatherapi

import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.web.client.RestTemplateBuilder
import org.springframework.stereotype.Service
import org.springframework.web.client.RestTemplate
import java.time.Duration

@Service
class WeatherProviderFactory {

    @Autowired
    lateinit var weatherProviderList: List<WeatherProvider>

    fun getWeatherProvider(providerName : String) : AbstractWeatherProvider? {
        for (aWeatherProvider in weatherProviderList){
            if(aWeatherProvider.isActive() && (aWeatherProvider.getName().equals(providerName, ignoreCase = true))){
                    return  when(providerName){
                        ProviderEnums.OPEN_WEATHER_MAP.providerName -> aWeatherProvider as OpenWeatherMapProvider
                        else -> null
                    }
            }
        }
        return null
    }
}

class OpenWeatherRestTemplateFactory(private val restTemplateBuilder: RestTemplateBuilder){

    val openWeatherRestTemplate : RestTemplate
    get() = restTemplateBuilder
        .setConnectTimeout(Duration.ofSeconds(3))
        .setReadTimeout(Duration.ofSeconds(3))
        .build()

}