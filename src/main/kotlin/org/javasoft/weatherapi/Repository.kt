package org.javasoft.weatherapi

import org.springframework.data.jpa.repository.JpaSpecificationExecutor
import org.springframework.data.repository.CrudRepository
import org.springframework.stereotype.Repository

@Repository
interface WeatherRecordRepository: CrudRepository<WeatherRecordEntity, Long>, JpaSpecificationExecutor<WeatherRecordEntity> {
}