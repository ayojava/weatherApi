package org.javasoft.weatherapi

import org.springframework.stereotype.Service

@Service
class WeatherDbService(private val weatherRecordRepository: WeatherRecordRepository) {

    fun saveWeatherEntity(weatherRecordEntity : WeatherRecordEntity){
        weatherRecordRepository.save(weatherRecordEntity)
    }

    fun getWeatherEntityList(weatherForecastRequest: WeatherForecastRequest): List<WeatherRecordEntity>? {
        val (cityId,cityName) = weatherForecastRequest
        val recordSpecification =
            WeatherRecordSpecification.buildWeatherRecordSpecification(cityId, cityName)
        return weatherRecordRepository.findAll(recordSpecification)
    }


}